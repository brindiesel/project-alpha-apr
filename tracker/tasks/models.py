from django.db import models
from django.contrib.auth.models import User
from tasks.models import Task

# Create your models here.

class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_complicated = models.BooleanField(default=False)
    project = models.ForeignKey(
        Task,
        related_name="tasks",
        on_delete=models.CASCADE
        )
    assignee = models.ForeignKey(
        User,
        null=True,
        related_name="tasks",
        on_delete=models.CASCADE
        )
