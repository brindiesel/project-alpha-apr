from django.urls import path, include
from projects.views import list_projects
from django.contrib import admin
from django.shortcuts import redirect

urlpatterns = [
    path("", list_projects, name="home"),
]
