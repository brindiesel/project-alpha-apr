from django.shortcuts import render
from .models import Project

# Create your views here.

def list_projects(request):
    project_lists = Project.objects.filter(owner=request.user)
    context = {
        "project": list_projects
    }
    return render(request, "projects/list.html", context)
