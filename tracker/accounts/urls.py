from django.urls import path
from accounts.views import login_user, user_logout

urlpattern = [
    path("login/", login_user, name="login_user"),
    path("logout/", user_logout, name="user_logout")
]
