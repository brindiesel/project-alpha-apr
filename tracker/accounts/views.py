from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, user_logged_in, user_logged_out
from .forms import LoginForm

def login_user(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")

    else:
        form = LoginForm()
    context = {
        "form": form
    }
    return render(request, "acounts/login.html", context)

def user_logout(request):
    logout(request)
    return redirect("list_projects")
